<?php //php start
/**
 * Plugin Name: bbest-consultation
 * Plugin URI: http://80000ft.duckdns.org
 * Description: This plugin has been developed in order to aid consultation processes for the BBEST neighbourhood planning forum.
 * Version: 1.00
 * Author: Graham Haley
 * Author URI: http://80000ft.duckdns.org
 * License: GPL2
 */
 
 function consult_shortcode() {
	html_form_code();
	response_mail();
 }
 
 add_shortcode( 'bbest_consultation_plugin', 'consult_shortcode' );
 
 function html_form_code() {
	echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
	echo '<p>';
	//Name
	echo 'Name <br />';
	echo '<input type="text" name="consult_Name" value="" size="50" />';
	echo '</p>';
	//e-mail
	echo '<p>';
	echo 'e-mail Address <br />';
	echo '<input type="email" name="consult_email" value="" size="50" />';
	echo '</br>Confirm e-mail Address <br />';
	echo '<input type="email" name="consult_email" value="" size="50" />';
	echo '</p>';
	//Address
	echo '<p>';
	echo '<b>Street Address</b></br>House name/number</br>';
	echo '<input type="text" name="consult_HouseNameNum" value="" size="50" /></br>';
	echo 'Street Name</br>';
	echo '<input type="text" name="consult_StreetName" value="" size="50" /></br>';
	echo 'Post Code</br>';
	echo '<input type="text" name="consult_PostCode" value="" size="8" /></br>';
	echo '</p>';
	//comments
	echo '<p>';
	echo 'Section 1 comments</br>';
	echo '<textarea rows="8" cols="50" name="consult_sect1" ></textarea></br>';
	echo 'Section 2 comments</br>';
	echo '<textarea rows="8" cols="50" name="consult_sect2" /></textarea></br>';
	echo '</p>';
	//Submit
	echo '<p><input type="submit" name="consult_submit" value="Send"/></p>';
	echo '</form>';
 }
 
 function response_mail() {
	 
 }
 
 ?>